require 'test_helper'

class PrioritiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @priority = priorities(:one)
  end

  test "should get index" do
    get priorities_url, as: :json
    assert_response :success
  end

  test "should create priority" do
    assert_difference('Priority.count') do
      post priorities_url, params: { priority: {  } }, as: :json
    end

    assert_response 201
  end

  test "should show priority" do
    get priority_url(@priority), as: :json
    assert_response :success
  end

  test "should update priority" do
    patch priority_url(@priority), params: { priority: {  } }, as: :json
    assert_response 200
  end

  test "should destroy priority" do
    assert_difference('Priority.count', -1) do
      delete priority_url(@priority), as: :json
    end

    assert_response 204
  end
end
