class AddNameToPriority < ActiveRecord::Migration[6.0]
  def change
    add_column :priorities, :name, :string
  end
end
