class AddForeignKeysToItems < ActiveRecord::Migration[6.0]
  def change
    add_reference :items, :status, foreign_key: true
    add_reference :items, :priority, foreign_key: true
  end
end
