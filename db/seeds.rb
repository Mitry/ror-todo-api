# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


statuses_list = [
    'active',
    'done',
    'deleted',
]

statuses_list.each do |n|
  Status.where(name: n).first_or_create
end

priorities_list = [
    'high',
    'medium',
    'low',
]

priorities_list.each do |n|
  Priority.where(name: n).first_or_create
end

user_list = [
  ["example@example.com", "qwerty"]
]

user_list.each do |a, b|
  User.where(email: a, encrypted_password: b).first_or_create
end